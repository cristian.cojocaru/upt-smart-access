
#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_PN532.h>
#include <AESLib.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Servo.h>

//configuratia de pini pentru modulul NFC PN532
#define PN532_IRQ   A4
#define PN532_RESET A5

//configuratia de pini pentru leduri
#define led_green_PIN 6
#define led_red_PIN 4
#define led_orange_PIN 5


byte lock = 0;  // setam un flag care sa indice starea usii, initial usa e setata pe 0 (LOCKED)


  uint8_t SELECT_APDU[] = {
    0x00, /* CLA */
    0xA4, /* INS */
    0x04, /* P1  */
    0x00, /* P2  */
    0x0F, /* Length of AID  */
    0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0xA0,0xA1,0xB0,0xB1,0xC0,0xC1,0xD0,0xD1,0xE0, /* AID  */
    0x00  /* Le  */};


    uint8_t APDU_COMMAND[] = {
    0x00, /* CLA */
    0xA4, /* INS */
    0x04, /* P1  */
    0x00, /* P2  */
    0x0F, /* Length of AID  */
    0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0xA0,0xA1,0xB0,0xB1,0xC0,0xC1,0xD0,0xD1,0xE1, /* AID  */
    0x00  /* Le  */};


// crearea de instante
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);
Servo servo;


 void setup(void) {

 //configurarea ledurilor
  pinMode(led_green_PIN, OUTPUT);
  pinMode(led_red_PIN, OUTPUT);
  digitalWrite(led_green_PIN, LOW);
  digitalWrite(led_red_PIN, LOW);
   pinMode(led_orange_PIN, OUTPUT);
  digitalWrite(led_orange_PIN, LOW);

  // configurarea usii ca fiind inchisa (180 - locked)
  servo.write(180);
  servo.attach(3);


  Serial.begin(115200);
  nfc.begin();

  // configure board to read RFID tags
  nfc.SAMConfig();

  
}

void loop(void) 
{

  digitalWrite(led_red_PIN, HIGH);
  Serial.println("Door IS LOCKED");
  lock = 1; //flag pus pe 1

  if (nfc.inListPassiveTarget()){ 


    uint8_t SELECT_RESP_buffer[40]={0};
    uint8_t APDU_RESP_buffer[40]={0};

    uint8_t expected_SEL_RESP[40]={57,48,57,48}; // valoare in bytes a stringului "9090"

    uint8_t Acces_Granted[]={57,48,48,48}; // valoare in bytes a stringului "9000"

    uint8_t SELECT_responseLength = sizeof(SELECT_RESP_buffer);
    uint8_t APDU_responseLength = sizeof(APDU_RESP_buffer);



    bool SEL_COMMAND_SUCCESS = nfc.inDataExchange(SELECT_APDU, sizeof(SELECT_APDU), SELECT_RESP_buffer, &SELECT_responseLength);


    if (SEL_COMMAND_SUCCESS) 
    {
      Serial.println("SEL AID succesfully sent to HCE");
      nfc.PrintHex(SELECT_APDU, sizeof(SELECT_APDU));
      Serial.println("Response from HCE is:");
      nfc.PrintHex(SELECT_RESP_buffer, SELECT_responseLength);
      
      int result = memcmp(SELECT_RESP_buffer, expected_SEL_RESP, sizeof(expected_SEL_RESP));
  
      if (result == 0) 
      {
        
        bool APDU_COMMAND_SUCCES = nfc.inDataExchange(APDU_COMMAND, sizeof(APDU_COMMAND), APDU_RESP_buffer, &APDU_responseLength);


        if (APDU_COMMAND_SUCCES) 
        {

          Serial.println("\n");
          Serial.println("APDU COMMAND succesfully sent to HCE");
          nfc.PrintHex(APDU_COMMAND, sizeof(SELECT_APDU));
          
          Serial.println("RESPONSE APDU from HCE is:");
          nfc.PrintHex(APDU_RESP_buffer, APDU_responseLength);
          
          
          int result2 = memcmp(APDU_RESP_buffer, Acces_Granted, sizeof(Acces_Granted));

          if(result2 == 0 && lock == 1)
          {
            Serial.println("\nAccess Granted");
            servo.write(90); // setam motorul pe unlock
            digitalWrite(led_red_PIN, LOW);
            digitalWrite(led_green_PIN, HIGH);
            digitalWrite(led_orange_PIN, LOW);
            
            Serial.println("DOOR UNLOCKED");        
            delay(4000); 
            digitalWrite(led_green_PIN, LOW); 
            servo.write(180); // dupa un scurt delay, usa se inchide din nou
            
          }
          

        } else{
          
          Serial.println("Access Denied");
          digitalWrite(led_orange_PIN, HIGH); 
          digitalWrite(led_green_PIN, LOW); 
          delay(1000);
          digitalWrite(led_orange_PIN, LOW);
        }
        
      }
      else{
            Serial.println("Access Denied");
            digitalWrite(led_orange_PIN, HIGH); 
            digitalWrite(led_green_PIN, LOW); 
            delay(1000);
            digitalWrite(led_orange_PIN, LOW);  
          }
      
    }
    else{
    
    Serial.println("Access Denied");
    digitalWrite(led_orange_PIN, HIGH); 
    digitalWrite(led_green_PIN, LOW); 
    delay(1000);
    digitalWrite(led_orange_PIN, LOW); 
    }
    
  }
  
}



